# Pasi pentru exemplificarea comenzilor git
1. Cream un folder local numit myCars
2. In interiorul acestuia cream un fisier **README.md**
3. Cream un repository git nou local <br>
	- vom folosi comanda: __*git init*__ <br>
4. Vom pune fisiuerul **README.md** in resository local 
	- vom folosi comanda: __*git add README.md*__
	- vom folosi comanda: __git commit -m "add README"__ <br>
5. Vom pune repository local in repository remote de pe serverul GitLab <br>
*Pentru aceasta vom face urmatoarele:
	- vom crea un proiect nou pe GitLab, numit **myCars_project** 
	- vom folosi comanda __*git remote add origin git@gitlab.com:Beia55/mycars_project.git*__
	- vom folosi comanda __*git push -u origin master*__
6. Acum, in interiorul folderului myCars, vom crea un nou folder numit **Opel**. Aici vom crea doua fisiere noi: **corsa.md** si **astra.md**.
7. Vom pune si aceste doua fisiere in remository local, folosind comanda: 
	- __*git add Opel*__, pentru a adauga tot folderul, sau
	- putem sa le punem separat fisierele fara folder folosind comanda: __*git add astra.md, corsa.md*__
	- adauga un commit folosind comanda: __git commit -m "add asta.md & corsa.md"__
8. Vom pune modificarile pe repository remote, folosind comanda: __*git push*__
9. Vom pune modificarile facute la fisierul **README** in repository local:
	- utilizand din nou comanda __*git add README.md*__ 
	- apoi vom face un commit -m utilizand comanda __*git commit "add modifications to README"*__
10. Pentru a pune modificarile din repository local in repository remote, vom folosi din nou comanda __*git push*__
11. In repository remote vom crea un folder nou numit **Dacia**. Aici, vom crea 2 fisiere noi numite __logan.md__ si __sandero.md__. Vom adauga la fiecare cate o scurta descriere.
11 . Acum, ca sa obtinem modificarile din repository remote in repository local
	- vom folosi comanda __*git pull*__